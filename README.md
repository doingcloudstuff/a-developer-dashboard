# A developer's dashboard

![](./developers-dashbaord.svg)

# Modifications made to cassandra-driver

It turns out, `cassandra-driver` creates a tmp directory in the background to house config files. For example, `./tmpb8acxunw`.

This can be seen in the python file `cassandra/datastax/cloud/__init__.py`:

```python
def read_cloud_config_from_zip(cloud_config, create_pyopenssl_context):
    secure_bundle = cloud_config['secure_connect_bundle']
    use_default_tempdir = cloud_config.get('use_default_tempdir', None)
    with ZipFile(secure_bundle) as zipfile:
        base_dir = tempfile.gettempdir() if use_default_tempdir else os.path.dirname(secure_bundle)
        tmp_dir = tempfile.mkdtemp(dir=base_dir)
        try:
            zipfile.extractall(path=tmp_dir)
            return parse_cloud_config(os.path.join(tmp_dir, 'config.json'), cloud_config, create_pyopenssl_context)
        finally:
            shutil.rmtree(tmp_dir)
```

AWS Lambda, however, only allows `/tmp` to be writable and so does not allow this. So, I had modified the `tmp_dir` to be `/tmp/tmpAstraDB`:

```python
def read_cloud_config_from_zip(cloud_config, create_pyopenssl_context):
    secure_bundle = cloud_config['secure_connect_bundle']
    use_default_tempdir = cloud_config.get('use_default_tempdir', None)
    with ZipFile(secure_bundle) as zipfile:
        tmp_dir = "/tmp/tmpAstraDB"
        os.mkdir(tmp_dir)
        try:
            zipfile.extractall(path=tmp_dir)
            return parse_cloud_config(os.path.join(tmp_dir, 'config.json'), cloud_config, create_pyopenssl_context)
        finally:
            shutil.rmtree(tmp_dir)
```
