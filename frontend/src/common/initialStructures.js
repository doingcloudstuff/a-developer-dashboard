import { v4 as uuid4 } from "uuid";

export function emptyCell() {
  return { id: uuid4(), value: "{}---\n<h2>empty cell</h2>\n\n<p>Hello</p>" };
}

export function emptyDashboard() {
  return {
    title: "Example console",
    cells: [emptyCell()],
  };
}

export const emptyCredsPair = {
  key: "",
  value: "",
};
