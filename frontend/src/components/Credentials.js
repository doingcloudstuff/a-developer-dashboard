import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronDown,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";

export default function Credentials({
  creds,
  addNewPair,
  removeCredPair,
  updateCredPair,
  updateCreds,
}) {
  const [minimize, setMinimize] = useState(true);
  const [hide, setHide] = useState(true);

  const toggleHide = () => {
    setHide(!hide);
  };

  const replaceLetters = (word) => {
    if (!hide) return word;

    return "*".repeat(word.length);
  };

  const toggleMinimize = () => {
    setMinimize(!minimize);
  };

  const handleKeyValueChange = (id, key, value) => {
    updateCredPair(id, key, value);
  };

  if (!creds) return <></>;
  return (
    <div className="credentials">
      <div className="credentials-header" style={{ height: "2rem" }}>
        <div style={{ display: "flex", alignItems: "center", gap: "1rem" }}>
          <FontAwesomeIcon
            icon={minimize ? faChevronRight : faChevronDown}
            style={{ cursor: "pointer" }}
            onClick={toggleMinimize}
          />
          <h2 className="credentials-header-name">Credentials</h2>
        </div>
        {minimize || <button onClick={addNewPair}>Add new pair</button>}
      </div>

      {minimize || (
        <div style={{ marginTop: "2rem" }}>
          {Object.entries(creds).map(([id, { key, value }]) => (
            <div key={id} className="cred-items">
              <input
                value={key}
                onChange={(e) =>
                  handleKeyValueChange(id, e.target.value, value)
                }
              />
              <input
                value={replaceLetters(value)}
                onChange={(e) => handleKeyValueChange(id, key, e.target.value)}
              />
              <button onClick={() => removeCredPair(id)}>x</button>
            </div>
          ))}

          <div className="credential-buttons">
            <button onClick={toggleHide}>{hide ? "Show" : "Hide"}</button>
            <button onClick={updateCreds}>Save</button>
          </div>
        </div>
      )}
    </div>
  );
}
