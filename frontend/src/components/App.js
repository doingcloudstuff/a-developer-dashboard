import { useState, useEffect } from "react";
import Header from "./Header";
import Dashboard from "./Dashboard";
import Credentials from "./Credentials";
import { v4 as uuid4 } from "uuid";
import {
  emptyDashboard,
  emptyCredsPair,
  emptyCell,
} from "../common/initialStructures";

function App() {
  const [user, setUser] = useState();
  const [apiKey, setApiKey] = useState();
  const [creds, setCreds] = useState({});
  const [dashboard, setDashboard] = useState(emptyDashboard());
  const core_url = "https://fufv43hq9d.execute-api.us-west-2.amazonaws.com/dev";

  // GET /api_key
  useEffect(() => {
    if (!user) return;
    let results;

    const getApiKey = async () => {
      let url = `${core_url}/api_key`;
      let options = {
        method: "GET",
        headers: {
          username: user,
          password: "password",
        },
      };
      let response = await fetch(url, options);
      if (response.ok) {
        results = await response.json();
        setApiKey(results.api_key);
      } else {
        console.error(response.status);
        console.error(response.text());
      }
    };

    getApiKey();
  }, [user]);

  // GET /credentials
  useEffect(() => {
    if (!apiKey) return;

    const getCreds = async () => {
      const url = `${core_url}/credentials`;
      const options = {
        method: "GET",
        headers: {
          api_key: apiKey,
        },
      };

      const response = await fetch(url, options);
      if (response.ok) {
        const results = await response.json();
        setCreds(results);
      } else {
        console.error(response.status);
        console.error(response.text());
      }
    };
    getCreds();
  }, [user, apiKey]);

  // POST /credentials
  const updateCreds = async () => {
    console.log("Update CREDS: ", creds);
    const url = `${core_url}/credentials`;
    const body = Object.values(creds);
    console.log("body: ", body);

    const options = {
      method: "POST",
      headers: {
        api_key: apiKey,
      },
      body: JSON.stringify({ creds: body }),
    };

    await fetch(url, options);
  };

  console.log("apiKey: ", apiKey);
  console.log("\n\ncreds: ", creds);

  const setTitle = (title) => {
    setDashboard((prev) => ({ ...prev, title }));
  };

  const updateCell = (id, value) => {
    setDashboard({
      title: dashboard.title,
      cells: [...dashboard.cells.filter((x) => x.id !== id), { id, value }],
    });
  };

  const removeCell = (id) => () => {
    setDashboard({
      title: dashboard.title,
      cells: dashboard.cells.filter((x) => x.id !== id),
    });
  };

  const addCell = () => {
    setDashboard({
      title: dashboard.title,
      cells: [...dashboard.cells, emptyCell()],
    });
  };

  const handleSignIn = () => {
    setUser("not-a-user");
  };

  const handleTitle = (event) => {
    setTitle(event.target.value);
  };

  const addNewPair = () => {
    setCreds((prev) => ({ ...prev, [uuid4()]: emptyCredsPair }));
  };

  const removeCredPair = (id) => {
    setCreds((prev) => {
      const { [id]: _id, ...rest } = { ...prev };
      return rest;
    });
  };

  const updateCredPair = (id, key, value) => {
    setCreds({ ...creds, [id]: { key, value } });
  };

  const handleSignOut = () => {
    setUser();
    setCreds({});
  };

  return (
    <>
      <div className="main">
        <Header
          className="header"
          title={dashboard.title}
          user={user}
          handleSignOut={handleSignOut}
          handleTitle={handleTitle}
          handleSignIn={handleSignIn}
        />
        <Credentials
          creds={creds}
          addNewPair={addNewPair}
          removeCredPair={removeCredPair}
          updateCredPair={updateCredPair}
          updateCreds={updateCreds}
        />
        <Dashboard
          creds={creds}
          cells={dashboard.cells}
          addCell={addCell}
          updateCell={updateCell}
          removeCell={removeCell}
        />
      </div>
    </>
  );
}

export default App;
