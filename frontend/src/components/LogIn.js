const LogIn = ({ user, handleSignIn, handleSignOut }) => {
  return (
    <div
      style={{
        paddingTop: "10px",
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
      }}
    >
      {user && (
        <p style={{ marginRight: ".5rem", fontSize: "1.5rem" }}>
          Welcome, {user}!
        </p>
      )}
      <button
        style={{ padding: ".3rem .6rem", margintLeft: "3rem" }}
        onClick={() => {
          user ? handleSignOut() : handleSignIn();
        }}
      >
        {user ? "Sign Out" : "Sign In"}
      </button>
    </div>
  );
};

export default LogIn;
