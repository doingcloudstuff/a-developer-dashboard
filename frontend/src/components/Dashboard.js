import Cell from "./Cell";
import { faAdd, faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Dashboard({
  creds,
  cells,
  addCell,
  updateCell,
  removeCell,
}) {
  return (
    <>
      {/* <div className="dashboard-header">
        <h2>Dashboard</h2>
        <button onClick={() => addCell()}>Add Cell</button>
      </div> */}
      {cells.map((cell) => (
        <div key={cell.id}>
          <div className="cell">
            <Cell
              id={cell.id}
              creds={creds}
              text={cell.value}
              addCell={addCell}
              updateCell={updateCell}
              removeCell={removeCell}
            />
          </div>
          <div className="cell-bottom-bar">
            <span className="delete-cell-button" onClick={removeCell(cell.id)}>
              <FontAwesomeIcon icon={faMinus} className="add-delete-cell" />
              Delete cell above
            </span>
            <span className="add-cell-button" onClick={addCell}>
              <FontAwesomeIcon icon={faAdd} className="add-delete-cell" />
              Add cell below
            </span>
          </div>
        </div>
      ))}
    </>
  );
}
