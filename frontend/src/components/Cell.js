import { useState, useEffect, useRef } from "react";
import JsxParser from "react-jsx-parser";
import DisplaySecret from "../jsx/DisplaySecret";

function Cell({ creds, text, addCell, updateCell, removeCell }) {
  // hooks
  const [content, setContent] = useState(text);
  const [editMode, setEditMode] = useState(false);
  const cellRef = useRef();

  useEffect(() => {
    // define callback for event listener
    const setEditModeToFalse = (event) => {
      console.log("setEditModeToFalse");
      if (cellRef.current?.contains(event.target)) return;
      setEditMode(false);
    };

    // add event listener
    document.body.addEventListener("click", setEditModeToFalse);

    // clean-up
    return () => document.body.removeEventListener("click", setEditModeToFalse);
  }, []);

  // functions
  const handleText = (e) => setContent(e.target.value);
  const toggleEditMode = () => setEditMode((editMode) => !editMode);

  // variables
  let bindings = {};
  let jsx = "<></>";
  let sep = 0;
  let end = -1;
  if (!editMode) {
    end = content.search("---");
    sep = Math.max(0, end);
    end = end === -1 ? 0 : end + 3;
    const bindingString = content.slice(0, sep).trim();
    bindings = eval(`new Object(${bindingString})`);
    Object.values(creds).forEach(({ key, value }) => {
      bindings[key] = value;
    });
    jsx = content.slice(end).trim();
  }

  console.log("editMode: ", editMode);
  console.log("bindings: ", bindings);
  console.log("creds: ", creds);
  console.log("content: ", content, sep);
  console.log("jsx: ", jsx);

  return (
    <>
      {editMode && (
        <textarea
          ref={cellRef}
          className="cell-edit-mode"
          value={content}
          onChange={handleText}
          autoFocus
          onFocus={(e) => (e.target.selectionStart = e.target.value.length)}
        />
      )}
      {editMode || (
        <div
          className="cell-render-mode"
          style={{ display: editMode ? "none" : "block", width: "100%" }}
          onDoubleClick={toggleEditMode}
        >
          <JsxParser
            bindings={bindings}
            components={{ DisplaySecret }}
            jsx={jsx}
          />
        </div>
      )}
    </>
  );
}

export default Cell;
