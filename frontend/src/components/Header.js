import LogIn from "./LogIn";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFloppyDisk, faFolderPlus } from "@fortawesome/free-solid-svg-icons";

export default function Header({
  user,
  title,
  handleTitle,
  handleSignIn,
  handleSignOut,
}) {
  return (
    <>
      <LogIn
        user={user}
        handleSignIn={handleSignIn}
        handleSignOut={handleSignOut}
      />
      <header
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <input value={title} onChange={handleTitle} />
        <FontAwesomeIcon
          icon={faFolderPlus}
          style={{ cursor: "pointer", marginRight: "1rem" }}
          onClick={() => {}}
        />
        <FontAwesomeIcon
          icon={faFloppyDisk}
          style={{ cursor: "pointer" }}
          onClick={() => {}}
        />
      </header>
    </>
  );
}
