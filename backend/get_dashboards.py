import json
from crud import get_dashboards


def lambda_handler(event: dict, context):
    print("event: ", json.dumps(event, default=str))
    headers = event["headers"]

    if not "api_key" not in headers:
        return {
            'statusCode': 400,
            'body': json.dumps({})
        }

    api_key = event['headers']['api_key']


    try:
        dashboards = get_dashboards(api_key=api_key)
    except Exception as err:
        print(err)
        return {
            'statusCode': 400,
            'body': json.dumps({})
        }

    return {
        'statusCode': 200,
            'body': json.dumps({'dashboards': dashboards})
    }
