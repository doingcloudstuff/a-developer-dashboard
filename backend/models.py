from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model


class User(Model):
    __keyspace__ = "dev"
    username = columns.Text(primary_key=True, required=True)
    password = columns.Text()
    api_key = columns.UUID()

class Credential(Model):
    __keyspace__ = "dev"
    api_key = columns.UUID(primary_key=True)
    key = columns.Text(primary_key=True, clustering_order="ASC")
    value = columns.Text()

class DashboardById(Model):
    __keyspace__ = "dev"
    id = columns.UUID(primary_key=True)
    api_key = columns.UUID()
    filename = columns.Text()
    bucket = columns.Text()
    key = columns.Text()

class DashboardByApiKey(Model):
    __keyspace__ = "dev"
    api_key = columns.UUID(primary_key=True)
    id = columns.UUID(primary_key=True, clustering_order="ASC")
    filename = columns.Text()
    bucket = columns.Text()
    key = columns.Text()
