import json
from crud import get_api_key


def lambda_handler(event: dict, context):
    print("event: ", json.dumps(event, default=str))
    headers = event["headers"]

    if not ("username" in headers and "password" in headers):
        return {
            'statusCode': 400,
            'body': "Authentication failure."
        }

    username = headers["username"]
    password = headers["password"]
    
    try:
        api_key = get_api_key(username=username, password=password)
        if not api_key:
            raise Exception
    except Exception as err:
        print(err)
        return {
            'statusCode': 400,
            'body': "Authentication failure."
        }

    return {
        'statusCode': 200,
        'body': json.dumps({
            "api_key": str(api_key)
        })
    }
