import json
from cassandra.cluster import Cluster, Session
from cassandra.auth import PlainTextAuthProvider
from cassandra.cqlengine.connection import register_connection, set_default_connection

CLUSTER_BUNDLE = "./secure-connect-dashboard.zip"

with open("./config.json", "r") as f:
    creds = json.load(f)

ASTRA_DB_CLIENT_ID = creds["ASTRA_DB_CLIENT_ID"]
ASTRA_DB_CLIENT_SECRET = creds["ASTRA_DB_CLIENT_SECRET"]


def get_cluster() -> Cluster:
    cloud_config = {"secure_connect_bundle": CLUSTER_BUNDLE}
    auth_provider = PlainTextAuthProvider(ASTRA_DB_CLIENT_ID, ASTRA_DB_CLIENT_SECRET)
    return Cluster(cloud=cloud_config, auth_provider=auth_provider)


def get_session() -> Session:
    cluster = get_cluster()
    session: Session = cluster.connect()
    register_connection(str(session), session=session)
    set_default_connection(str(session))
    return session