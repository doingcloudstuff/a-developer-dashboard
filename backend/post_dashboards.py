import json
from crud import create_dashboard


def lambda_handler(event: dict, context):
    print("event: ", json.dumps(event, default=str))
    headers = event["headers"]

    if not ("api_key" in headers and "body" in event):
        return {
            'statusCode': 400,
            'body': json.dumps({})
        }

    api_key = event['headers']['api_key']
    body = json.loads(event['body'])


    try:
        success = create_dashboard(api_key=api_key, creds=body)
    except Exception as err:
        print(err)
        return {
            'statusCode': 400,
            'body': json.dumps({"success": False})
        }

    return {
        'statusCode': 200,
            'body': json.dumps({"success": True})
    }
