from uuid import uuid4 as uuid, UUID
from cassandra.cqlengine.management import sync_table
from db import get_session
from models import User, Credential, DashboardByApiKey, DashboardById

session = get_session()
sync_table(User)
sync_table(Credential)
sync_table(DashboardByApiKey)
sync_table(DashboardById)

# USER
def create_user(username: str, password: str) -> User:
    return User.create(username=username, password=password, api_key=uuid())

def get_api_key(username: str, password: str) -> UUID:
    """GET /api_key."""
    try:
        user: User = User.filter(username=username).get()
    except Exception as err:
        print(err)
        return None

    if user.password != password:
        return None
    
    return user.api_key

# CREDENTIALS
def get_creds(api_key: str) -> dict:
    """GET /credentials."""
    creds = Credential.filter(api_key=UUID(api_key)).all()
    return {str(uuid()): {"key": cred.key, "value": cred.value} for cred in creds}

def create_cred(api_key: str, key: str, value: str) -> Credential:
    return Credential.create(api_key=UUID(api_key), key=key, value=value)

def create_creds(api_key: str, creds: list):
    """POST /credentials."""
    for x in creds:
        create_cred(api_key=api_key, key=x["key"], value=x["value"])


# DASHBOARDS
def get_dashboards(api_key: str) -> list:
    """GET /dashboards."""
    try:
        dashboards = DashboardByApiKey.filter(api_key=UUID(api_key)).all()
    except Exception as err:
        print(err)
        return None

    return [
        {
            "filename": dashboard.filename,
            "id": str(dashboard.id),
            "bucket": dashboard.bucket,
            "key": dashboard.key
        }
        for dashboard in dashboards
    ]

def get_dashboard(api_key: str, id: str) -> dict:
    """GET /dashboards/{id}."""
    try:
        dashboard = DashboardById.filter(id=UUID(id)).get()
    except Exception as err:
        print(err)
        return None

    if str(dashboard.api_key) != api_key:
        return None

    return {
        "filename": dashboard.filename,
        "id": str(dashboard.id),
        "bucket": dashboard.bucket,
        "key": dashboard.key
    }

def create_dashboard(api_key: str, bucket: str, key: str, filename: str, id: str = None) -> bool:
    """POST /dashboards."""

    api_uuid = UUID(api_key)
    dashboard_id = UUID(id) if id else uuid()
    try:
        DashboardByApiKey.create(api_key=api_uuid, bucket=bucket, key=key, filename=filename, id=dashboard_id)
        DashboardById.create(api_key=api_uuid, bucket=bucket, key=key, filename=filename, id=dashboard_id)
    except Exception as err:
        print(err)
        return False

    return True