import json
from crud import get_dashboard


def lambda_handler(event: dict, context):
    print("event: ", json.dumps(event, default=str))
    headers = event["headers"]

    if not ("api_key" in headers and "pathParameters" in event):
        return {
            'statusCode': 400,
            'body': json.dumps({})
        }

    api_key = event['headers']['api_key']
    id = event['pathParameters'].get('id', '')


    try:
        dashboard = get_dashboard(api_key=api_key, id=id)
    except Exception as err:
        print(err)
        return {
            'statusCode': 400,
            'body': json.dumps({})
        }

    return {
        'statusCode': 200,
            'body': json.dumps(dashboard)
    }
