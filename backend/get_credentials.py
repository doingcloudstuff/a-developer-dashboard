import json
from crud import get_creds


def lambda_handler(event: dict, context):
    print("event: ", json.dumps(event, default=str))

    if "api_key" not in event["headers"]:
        return {
            'statusCode': 400,
            'body': json.dumps({})
        }

    api_key = event['headers']['api_key']

    try:
        credentials = get_creds(api_key)
    except Exception as err:
        print(err)
        return {
            'statusCode': 400,
            'body': json.dumps({})
        }

    return {
        'statusCode': 200,
        'body': json.dumps(credentials)
    }
