import json
import crud


def lambda_handler(event: dict, context):
    print("event: ", json.dumps(event, default=str))
    
    user_cred = {
        "username": "not-a-user",
        "password": "password"
    }
    user = crud.create_user(**user_cred)
    print("user: ", dict(user))